# Statflo's Spiders

Spiders for [AT&T's](https://www.att.com/maps/store-locator.html) and [Verizon's](https://verizonwireless.com/stores/) stores.

## Getting Started

### Prerequisites

* [git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
* [python 3](https://docs.python.org/3/using/index.html)
* [scrapy](https://doc.scrapy.org/en/latest/intro/install.html)
* [shub](https://helpdesk.scrapinghub.com/support/solutions/articles/22000204081-deploying-your-spiders-to-scrapy-cloud) (optionally, for deployment to scrapinghub.com)

### Installing

Clone project:

```
git clone git@bitbucket.org:nsalikov/statflo.git
```

Change directory and install requirements:

```
cd statflo
pip install -r requirements.txt
```

If you have both python 2 and python 3 installed, use pip3 instead:

```
cd statflo
pip3 install -r requirements.txt
```

## Usage

You can specify output and log files:

```
scrapy crawl att --output=statflo/data/att.csv --logfile=statflo/data/var/log/att.log
scrapy crawl verizon --output=statflo/data/verizon.csv --logfile=statflo/data/var/log/verizon.log
```

To enable [persistence support](https://doc.scrapy.org/en/latest/topics/jobs.html) you need to define job directory:

```
scrapy crawl att --output=statflo/data/att.csv --logfile=statflo/data/var/log/att.log -s JOBDIR=statflo/data/var/job/att
```

## How to Speed Things Up

Note that the current settings are very polite (e.g slow). To speed things up you need to [configure](https://doc.scrapy.org/en/latest/topics/settings.html) Scrapy through `settings.py` file.

### Reduce Delay, Increase Concurrency

```
CONCURRENT_REQUESTS = 16
DOWNLOAD_DELAY = 3
CONCURRENT_REQUESTS_PER_DOMAIN = 16
```
You can reduce (e.g. `DOWNLOAD_DELAY = 0.5`) or even disable (`DOWNLOAD_DELAY = 0`) download delay. Simultaneously, you can increase request concurrency.

### Use Proxies

In order to use proxies, you need to prepare file with list of proxies:

```
PROXY_LIST = 'data/proxies.txt'
```

Then uncomment last two lines:


```
DOWNLOADER_MIDDLEWARES = {
    'scrapy_fake_useragent.middleware.RandomUserAgentMiddleware': 80,
    'scrapy.downloadermiddlewares.retry.RetryMiddleware': 90,
    # 'scrapy_proxies.RandomProxy': 100,
    # 'scrapy.downloadermiddlewares.httpproxy.HttpProxyMiddleware': 110,
}
```

Unfortunately, public proxies have proven themselves pretty unrelaible (even pool of 300+ proxies). So will be better to pick up 5-10 paid proxies.

### Scrapinghub.com?

According to scrapinghub's [pricing](https://scrapinghub.com/scrapy-cloud), free account allow you to get 1 concurrent crawl. So you need to buy access to [expensive](https://scrapinghub.com/crawlera) Crawlera downloader for more concurrency.
