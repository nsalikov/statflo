# Automatically created by: shub deploy

from setuptools import setup, find_packages

setup(
    name         = 'statflo',
    version      = '1.0',
    packages     = find_packages(),
    entry_points = {'scrapy': ['settings = statflo.settings']},
    package_data={
        'statflo': ['data/*.txt', 'data/*.csv']
    },
    zip_safe=False,
)
