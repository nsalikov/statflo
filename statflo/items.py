# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class StatfloItem(scrapy.Item):
    Name = scrapy.Field()
    Address = scrapy.Field()
    City = scrapy.Field()
    State = scrapy.Field()
    Country = scrapy.Field()
    Phone = scrapy.Field()
    Zipcode = scrapy.Field()
    Authorized = scrapy.Field()
    Dealer = scrapy.Field()
    Url = scrapy.Field()


    def __repr__(self):
        if 'Phone' in self:
            return repr({"Phone": self["Phone"]})
        else:
            return ""