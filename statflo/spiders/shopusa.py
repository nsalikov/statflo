# -*- coding: utf-8 -*-
import scrapy
import json
from statflo.items import StatfloItem


class ShopusaSpider(scrapy.Spider):
    name = 'shopusa'
    allowed_domains = ['shopusawireless.com']
    start_urls = ['http://shopusawireless.com/graphics/js/locations.txt']

    def parse(self, response):
        if not response.body.startswith(b'[{'):
            self.logger.warning('Something may be wrong with <{}>'.format(response.request.url))
            return

        json_text = response.text

        try:
            data = json.loads(json_text)
        except:
            self.logger.warning('Could not parse json file: {}'.format(json_text))
            return

        for store in data:
            item = StatfloItem()

            if 'name' in store:
                item['Name'] = store['name'].strip()
            if 'address' in store:
                item['Address'] = store['address'].strip()
            if 'city' in store:
                item['City'] = store['city']
            if 'state' in store:
                item['State'] = store['state'].strip()
            item['Country'] = 'United States'
            if 'phone' in store:
                try:
                    # 631.467.8088 => (631) 467-8088
                    item['Phone'] = '({}) {}-{}'.format(*store['phone'].strip().split('.'))
                except:
                    self.logger.warning('Could not parse phone number: {}'.format(store['phone']))
                    item['Phone'] = store['phone'].strip()
            if 'zip' in store:
                item['Zipcode'] = store['zip'].strip()
            item['Authorized'] = 'Yes'
            item['Dealer'] = 'AT&T Authorized Retailer'
            # item['Url'] = response.url

            yield item
