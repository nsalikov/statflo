# -*- coding: utf-8 -*-
import scrapy
import json
from statflo.items import StatfloItem


class CricketSpider(scrapy.Spider):
    name = 'cricket'
    allowed_domains = ['cricketwireless.com', 'momentfeed-prod.apigee.net']
    # ATTENTION! Criket auth token may be invalid/expired.
    start_urls = ['https://momentfeed-prod.apigee.net/api/llp/cricket.json?auth_token=IVNLPNUOBXFPALWE&center=41.2524,-95.998&coordinates=-5.9657536710655235,2.8125,69.03714171275197,-194.765625&name=Cricket+Wireless+Authorized+Retailer,Cricket+Wireless+Store&page=1&pageSize=50&type=store']


    def parse(self, response):
        if not response.body.startswith(b'['):
            self.logger.error('Could not parse server\'s response. Check url, auth_token and server\'s response')

        json_text = response.text

        try:
            data = json.loads(json_text)
        except:
            self.logger.warning('Could not parse json file: {}'.format(json_text))
            return

        for store in [store['store_info'] for store in data if 'store_info' in store]:
            item = StatfloItem()

            item['Name'] = 'NA'
            if 'address' in store:
                item['Address'] = store['address'].strip()
                if 'address_extended' in store and store['address_extended']:
                    item['Address'] = item['Address'] + ', ' + store['address_extended'].strip()
            if 'locality' in store:
                item['City'] = store['locality'].strip()
            if 'region' in store:
                item['State'] = store['region'].strip()
            item['Country'] = 'United States'
            if 'phone' in store:
                phone = store['phone'].strip()
                if not phone.startswith('(') and len(phone) == 10:
                    item['Phone'] = '({}) {}-{}'.format(''.join(phone[0:3]), ''.join(phone[3:6]), ''.join(phone[6:11]))
                else:
                    item['Phone'] = phone
            if 'postcode' in store:
                item['Zipcode'] = store['postcode'].strip()
            item['Dealer'] = 'Cricket Wireless Authorized Retailer'
            item['Authorized'] = 'Yes'
            # if 'url' in store:
            #     item['Url'] = store['url'].strip()

            yield item