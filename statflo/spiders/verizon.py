# -*- coding: utf-8 -*-
import scrapy
import json
import csv
from statflo.items import StatfloItem
from scrapy.shell import inspect_response


class VerizonSpider(scrapy.Spider):
    name = 'verizon'
    allowed_domains = ['verizon.com']
    url = 'https://www.verizonwireless.com/stores/storesearchresults/?allow=1&lat={lat}&long={lng}&result=all&q={zipcode}'
    csvfields = ['Zipcode', 'City', 'State', 'State Abbreviation', 'County', 'Latitude', 'Longitude']


    def start_requests(self):
        if 'ZIPCODES' not in self.settings:
            raise CloseSpider("You must specificy ZIPCODES' file")
        with open(self.settings['ZIPCODES'], encoding='utf8') as csvfile:
            dr = csv.DictReader(csvfile, fieldnames=self.csvfields)
            next(dr) # skip header
            for line in dr:
                yield scrapy.FormRequest(self.url.format(lat=line['Latitude'], lng=line['Longitude'], zipcode=line['Zipcode']),
                                            callback=self.parse,
                                            errback=self.make_new_request)


    def make_new_request(self, failure):
        self.logger.info('Make new request <{url}>'.format(url=failure.request.url))
        return scrapy.Request(url=failure.request.url, callback=self.parse, errback=self.make_new_request, dont_filter=True)


    def parse(self, response):
        json_text = ''

        for script in response.css('#storeLocator > script::text').extract():
            script = script.strip()
            if script.startswith('var resultsList='):
                json_text = script.replace('var resultsList=', '').strip()

        if not json_text:
            self.logger.warning('Something may be wrong <{}>'.format(response.url))
            # inspect_response(response, self)
            yield scrapy.Request(url=response.url, callback=self.parse, errback=self.make_new_request, dont_filter=True)
        else:
            for item in self.get_items(json_text):
                yield item


    def get_items(self, json_text):
        try:
            data = json.loads(json_text)
        except:
            return
        for store in data:
            item = StatfloItem()

            if 'title' in store:
                item['Name'] = store['title'].strip()
            if 'address' in store:
                item['Address'] = store['address'].strip()
            if 'city' in store:
                item['City'] = store['city']
            if 'stateAbbr' in store:
                item['State'] = store['stateAbbr'].strip()
            item['Country'] = 'United States'
            if 'phone' in store:
                try:
                    # 631.467.8088 => (631) 467-8088
                    item['Phone'] = '({}) {}-{}'.format(*store['phone'].strip().split('.'))
                except:
                    self.logger.warning('Could not parse phone number: {}'.format(store['phone']))
                    item['Phone'] = store['phone'].strip()
            if 'zip' in store:
                item['Zipcode'] = store['zip'].strip()
            if 'typeOfStore' in store:
                dealer = ', '.join(store['typeOfStore'])
                item['Dealer'] = dealer
                item['Authorized'] = 'Yes' if 'Authorized' in item['Dealer'] else 'No'
            # if 'storeUrl' in store:
            #     item['Url'] = store['storeUrl'].strip()

            yield item
