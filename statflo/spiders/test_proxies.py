# -*- coding: utf-8 -*-
import scrapy
from scrapy.shell import inspect_response

class TestProxiesSpider(scrapy.Spider):
    name = 'test-proxies'
    allowed_domains = ['http://www.whatismyproxy.com/']
    repeat = 10
    start_urls = ['http://www.whatismyproxy.com/'] * repeat

    def parse(self, response):
        if response.css('.siteHeaderShadow').extract_first():
            status, ip = response.css('p.h1::text').extract()
            ip = ip.replace('IP address:', '').strip()
            yield {ip : status}
            # inspect_response(response, self)
        else:
            yield Request(url=response.url, dont_filter=True)
