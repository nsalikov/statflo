# -*- coding: utf-8 -*-
import scrapy
import json
import csv
from statflo.items import StatfloItem
from scrapy.shell import inspect_response


class AttSpider(scrapy.Spider):
    name = 'att'
    allowed_domains = ['att.com']
    url = "https://www.att.com/apis/maps/v2/locator/search/query.json?q={zipcode}&poi_types=pos&radius=50"
    csvfields = ['Zipcode', 'City', 'State', 'State Abbreviation', 'County', 'Latitude', 'Longitude']


    def start_requests(self):
        if 'ZIPCODES' not in self.settings:
            raise CloseSpider("You must specificy ZIPCODES' file")
        with open(self.settings['ZIPCODES'], encoding='utf8') as csvfile:
            dr = csv.DictReader(csvfile, fieldnames=self.csvfields)
            next(dr) # skip header
            for line in dr:
                yield scrapy.Request(url=self.url.format(zipcode=line['Zipcode']), callback=self.parse, errback=self.make_new_request)


    def make_new_request(self, failure):
        self.logger.info('Make new request <{url}>'.format(url=failure.request.url))
        return scrapy.Request(url=failure.request.url, callback=self.parse, errback=self.make_new_request, dont_filter=True)


    def parse(self, response):
        if response.body.startswith(b'{"copyright"'):
            for item in self.get_items(response.body):
                yield item
        else:
            # inspect_response(response, self)
            yield scrapy.Request(url=response.url, callback=self.parse, errback=self.make_new_request, dont_filter=True)


    def get_items(self, body):
        try:
            data = json.loads(body)
        except:
            return
        if 'results' not in data:
            return
        for store in data['results']:
            item = StatfloItem()

            if 'mystore_name' in store:
                item['Name'] = store['mystore_name'].strip()
            if 'address1' in store:
                item['Address'] = store['address1'].strip()
                if 'address2' in store:
                    item['Address'] = item['Address'] + ', ' + store['address2'].strip()
            if 'city' in store:
                item['City'] = store['city']
            if 'region' in store:
                item['State'] = store['region'].strip()
            item['Country'] = 'United States'
            if 'phone' in store:
                item['Phone'] = store['phone'].strip()
            if 'postal' in store:
                item['Zipcode'] = store['postal'].strip()
            if 'name' in store:
                item['Dealer'] = store['name'].strip()
                item['Authorized'] = 'Yes' if 'Authorized' in item['Dealer'] else 'No'
            # if 'url' in store:
            #     item['Url'] = store['url'].strip()

            yield item
