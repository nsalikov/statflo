# -*- coding: utf-8 -*-
import scrapy
import re
from statflo.items import StatfloItem
from scrapy.shell import inspect_response


class ConnectSpider(scrapy.Spider):
    name = 'connect'
    allowed_domains = ['connectwireless.com']
    start_urls = ['http://connectwireless.com/']
    non_decimal = re.compile('[^\d]+')


    def parse(self, response):
        for link in response.css('.folder-parent a::attr(href)').extract():
            yield response.follow(link, callback=self.parse_state)


    def parse_state(self, response):
        # inspect_response(response, self)
        for store in response.css('div.sqs-block-content > h3 + p'):
            # self.logger.info('Content: {}'.format(store.extract()))
            rows = store.css('p:nth-child(2) *::text').extract()

            if len(rows) < 3:
                self.logger.warning('Not enough data: {}'.format(rows))
                continue

            address = rows[0].strip()

            address2 = rows[1].replace('.', '').strip()
            match = re.search('(.+?),\s*(\w+)\s+(\d+)', address2, re.DOTALL)
            if not match:
                self.logger.warning('Could not parse city, state and zip: {}'.format(rows))
                continue

            city = match.group(1)
            state = match.group(2)
            zipcode = match.group(3)

            phone = self.non_decimal.sub('', rows[2])
            if len(phone) != 10:
                self.logger.warning('Could not parse phone number: {}'.format(phone))
                continue

            phone = '({}) {}-{}'.format(''.join(phone[0:3]), ''.join(phone[3:6]), ''.join(phone[6:11]))

            item = StatfloItem()

            item['Name'] = 'NA'
            item['Address'] = address
            item['City'] = city
            item['State'] = state
            item['Country'] = 'United States'
            # 6314678088 => (631) 467-8088
            item['Phone'] = phone
            item['Zipcode'] = zipcode
            item['Dealer'] = 'AT&T Authorized Retailer'
            item['Authorized'] = 'Yes'
            # item['Url'] = response.url

            yield item