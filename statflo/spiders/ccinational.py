# -*- coding: utf-8 -*-
import scrapy
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor
from statflo.items import StatfloItem
from scrapy.shell import inspect_response
import json
import re


class CcinationalSpider(CrawlSpider):
    name = 'ccinational'
    allowed_domains = ['ccinational.com']
    start_urls = ['https://ccinational.com/Store-Index']

    rules = (
        Rule(LinkExtractor(restrict_css=(['div.store-item', 'div.store-wrp'])), callback='parse_item', follow=True),
    )


    def parse_item(self, response):
        if not re.match('.*ccinational.com/Store-Index/.+?/.+?/.+?', response.url):
            return

        match = None

        for script in response.css('#ctl01 > script::text').extract():
        	if 'JSON.stringify' in script:
		        match = re.search('JSON.stringify\((.+?)\)\);//', script, re.DOTALL)

        if not match:
            self.logger.warning('Could not parse embedded javascript: {}'.format(script))
            return

        json_text = match.group(1)

        try:
            store = json.loads(json_text)
        except:
            self.logger.warning('Could not parse json from embedded javascript: {}'.format(json_text))
            return

        item = StatfloItem()

        item['Name'] = 'NA'
        if 'Address' in store:
            item['Address'] = store['Address'].strip()
        if 'City' in store:
            item['City'] = store['City'].strip()
        if 'State' in store:
            item['State'] = store['State'].strip()
        item['Country'] = 'United States'
        if 'Phone' in store:
            item['Phone'] = store['Phone'].strip()
        if 'PostalCode' in store:
            item['Zipcode'] = store['PostalCode'].strip()
        item['Authorized'] = 'Yes'
        item['Dealer'] = 'AT&T Authorized Retailer'
        # item['Url'] = response.url

        yield item