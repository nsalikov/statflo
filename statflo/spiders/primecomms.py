# -*- coding: utf-8 -*-
import scrapy
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor
from statflo.items import StatfloItem
from scrapy.shell import inspect_response


class PrimecommsSpider(CrawlSpider):
    name = 'primecomms'
    allowed_domains = ['primecomms.com']
    start_urls = ['http://www.primecomms.com/retail-locations']

    rules = (
        Rule(LinkExtractor(allow=('.*primecomms.com/.+?/.+?/.+?'), restrict_css='.c-directory-list'), callback='parse_item'),
        Rule(LinkExtractor(restrict_css='.c-directory-list')),
    )


    def parse_item(self, response):
        item = StatfloItem()

        item['Name'] = 'NA'
        item['Address'] = response.css('#address .c-address-street *::text').extract_first().strip()
        item['City'] = response.css('#address .c-address-city *::text').extract_first().strip()
        item['State'] = response.css('#address .c-address-state *::text').extract_first().strip()
        item['Country'] = 'United States'
        item['Phone'] = response.css('#telephone *::text').extract_first().replace('Tel:', '').strip()
        item['Zipcode'] = response.css('#address .c-address-postal-code *::text').extract_first().strip()
        item['Authorized'] = 'Yes'
        item['Dealer'] = 'AT&T Authorized Retailer'
        # item['Url'] = response.url

        phone = item['Phone']
        if phone and not phone.startswith('('):
            try:
                phone = '({}) {}-{}'.format(*phone.split('-'))
            except:
                self.logger.warning('Could not parse phone number: {}'.format(phone))
        item['Phone'] = phone

        yield item