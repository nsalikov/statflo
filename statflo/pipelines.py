# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html

from scrapy.exceptions import DropItem


class StatfloPipeline(object):

    def process_item(self, item, spider):
        return item


class DuplicatesPipeline(object):

    def __init__(self):
        self.seen = set()


    def process_item(self, item, spider):
        if 'Phone' not in item or item['Phone'] == '':
            raise DropItem("Phone not found:")
        if item['Phone'] in self.seen:
            raise DropItem("Duplicate phone:")
        else:
            self.seen.add(item['Phone'])
            return item
