import asyncio
from proxybroker import Broker

types = [('HTTP', ('Anonymous', 'High')), ('HTTPS', ('Anonymous', 'High'))]
limit = 500

async def show(proxies):
    while True:
        proxy = await proxies.get()
        if proxy is None: break
        print(form_line(proxy))

def form_line(proxy):
    t = 'http'
    if 'HTTPS' in proxy.types and proxy.types['HTTPS']:
    	t = 'https'
    h = proxy.host
    p = proxy.port
    return '{type}://{host}:{port}'.format(type=t, host=h, port=p)

proxies = asyncio.Queue()
broker = Broker(proxies)
tasks = asyncio.gather(
    broker.find(types=types, limit=limit),
    show(proxies))

loop = asyncio.get_event_loop()
loop.run_until_complete(tasks)