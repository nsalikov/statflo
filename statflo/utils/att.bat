@echo off
SET agent="Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36"
SET url="https://www.att.com/apis/maps/v2/locator/search/query.json?q=06360&poi_types=pos&radius=50"

curl -A %agent% %url% | jq ".results|.[]|.mystore_name,.name,.url"