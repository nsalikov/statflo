@echo off
SET agent="Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36"
SET url="https://www.verizonwireless.com/stores/storesearchresults/?allow=1&lat=34.1265&long=-81.2348&result=all&q=29002"

curl -A %agent% %url% | python verizon_json.py | jq ".[]|.title,.storeUrl,.phone"