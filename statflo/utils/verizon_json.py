import sys
import lxml.html

stdin = sys.stdin.read()
page = lxml.html.fromstring(stdin)
script = page.cssselect('#storeLocator > script:nth-child(5)')[0].text_content()
json = script.replace('var resultsList=', '').strip()

print(json)